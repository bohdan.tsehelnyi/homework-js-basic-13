// ================THEORY=========================
/*
1. Яке призначення методу event.preventDefault() у JavaScript?

Метод event.preventDefault() реалізує відміну дії, яка задана обєкту в браузері. Наприклад натискання на посилання яке за замовчуванням відправляє нас по URL адресі. Таку дію ми можемо відмінити з допомогою preventDefault() та написати свою дію.

2. В чому сенс прийому делегування подій?

В оптимізацї обробки великого масиву обєктів, які мають спільну дію та спільного предка. До цієї теми нам потрібно було б виводити кожен обєкт та вішати на нього подію. А терпер ми можемо вішати подію на дочірні обєкти предка. Такий результат ми отримаємо з роботою метода event.target який ми використовуємо в предку для звернення до його дочірніх елементів.

3. Які ви знаєте основні події документу та вікна браузера?

Події документа:
DOMContentLoaded - коли HTML розмітку завантажено й оброблено, DOM документ повністю побудований та доступний.

Подія браузера:

onload - загрузка стороннього ресурса.
onerror - вивід помилки при загрузці стороннього ресурсу.
*/

// ================PRACTICE=======================

const button = document.querySelector('.tabs');
const textContent = document.querySelector('.tabs-content');

button.addEventListener("click", function(event) {
  let target = event.target;
  if (!target) return;

  let btn = target.dataset.active;

  target.classList.toggle('highlight');

  for(let text of textContent.children){  
    if(btn == text.dataset.active){
      text.classList.toggle('open');
    }}
});

